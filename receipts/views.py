from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {
        'receipt_list': receipt,
    }
    return render(request, "receipts/list.html", context)

@login_required
def expense_list(request):
    expense = ExpenseCategory.objects.all()
    context = {
        "expense_list": expense
    }
    return render(request, "categories/expense.html", context)

@login_required
def account_list(request):
    account = Account.objects.all()
    context = {
        "account_list": account
    }
    return render(request, "accounts/account.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "create_receipt": form,
    }
    return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.purchaser = request.user
            category.save()
            return redirect("categories")
    else:
        form = CategoryForm()
    context = {
        "category": form
    }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.purchaser = request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()
    context = {
        "create_account": form
    }
    return render(request, "accounts/create.html", context)
