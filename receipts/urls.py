from django.urls import path
from .views import receipt_list, create_receipt, expense_list, account_list, create_category, create_account

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_list, name="categories"),
    path("accounts/", account_list, name="accounts"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
